import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public formData: User;
  public userList: User[];
  readonly rootUrl = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  postUser(user: User) {
    return this.http.post(this.rootUrl + '/user', this.formData);
  }

  getUsersRefresh() {
    this.http.get(this.rootUrl + '/allUsers')
    .toPromise().then(res => this.userList = res as User[]);
  }

  getAll() {
    return this.http.get<User[]>(this.rootUrl + `/users`);
  }

  putUser(user: User) {
    return this.http.put(this.rootUrl + '/user', this.formData);
  }

  deleteUser(user: User) {
    return this.http.delete(this.rootUrl + '/user' + '/' + user.userId);
  }
}
