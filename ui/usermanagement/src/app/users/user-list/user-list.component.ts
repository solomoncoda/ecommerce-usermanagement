import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { User } from 'src/app/models/user.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private service: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.getUsersRefresh();
  }

  populateUserForm(user: User) {
    this.service.formData = Object.assign({}, user);
  }

  onDelete(user: User) {
    this.service.deleteUser(user).subscribe(res => {
      this.toastr.success('User Deleted.', 'eCommerce');
      this.service.getUsersRefresh();
    });
  }

}
