package com.example.usermanagement.controller;

import com.example.usermanagement.models.User;
import com.example.usermanagement.repository.UserRepository;
import com.example.usermanagement.service.UserService;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.validation.ValidationException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService mockUserService;

    @Mock
    private UserRepository mockUserRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeEach
    void testSetUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void testCreateValidUser() throws Exception {
        User mockUser = new User("name", "password", "address", "1234567890", "user@user.com");
        String userJson = "{\"userName\":\"name\",\"password\":\"password\",\"postalAddress\":\"address\",\"contactNumber\":\"1234567890\",\"email\":\"user@user.com\"}";
        Mockito.when(mockUserRepository.existsByuserName("name")).thenReturn(false);
        when(mockUserService.CreateUser(mockUser)).thenReturn(true);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/user")
                .accept(MediaType.APPLICATION_JSON).content(userJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testCreateInvalidUser() throws Exception {
        String userJson = "{\"userName\":\"name\",\"password\":\"password\",\"postalAddress\":\"address\",\"contactNumber\":\"1234567890\",\"email\":\"user@user.com\"}";
        Mockito.when(mockUserRepository.existsByuserName("name")).thenReturn(true);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/user")
                .accept(MediaType.APPLICATION_JSON).content(userJson)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isConflict());
    }

    @Test
    public void testUpdateUser() throws Exception {
        User mockUser = new User("name", "password", "address", "1234567890", "user@user.com");
        String userJson = "{\"userName\":\"name\",\"password\":\"password\",\"postalAddress\":\"address\",\"contactNumber\":\"1234567890\",\"email\":\"user@user.com\"}";
        Mockito.when(mockUserRepository.save(mockUser)).thenReturn(mockUser);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/user")
                .accept(MediaType.APPLICATION_JSON).content(userJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testDeleteUser() throws Exception {
        User mockUser = new User(1,"name", "password", "address", "1234567890", "user@user.com");
        String userJson = "{\"userId\":1,\"userName\":\"name\",\"password\":\"password\",\"postalAddress\":\"address\",\"contactNumber\":\"1234567890\",\"email\":\"user@user.com\"}";
        //when(mockUserRepository.deleteById(1)).thenReturn(true);


        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/user/1")
                .accept(MediaType.APPLICATION_JSON).content(userJson)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        verify(mockUserRepository, times(1)).deleteById(eq(1));
        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testGetUser() throws Exception {
        User mockUser = new User(1,"name", "password", "address", "1234567890", "user@user.com");
        String userJson = "{\"userId\":1,\"userName\":\"name\",\"password\":\"password\",\"postalAddress\":\"address\",\"contactNumber\":\"1234567890\",\"email\":\"user@user.com\"}";
        //when(mockUserRepository.deleteById(1)).thenReturn(true);
        Mockito.when(mockUserRepository.findByuserName(anyString())).thenReturn(mockUser);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/user")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testGetAllUser() throws Exception {
        User mockUser = new User(1,"name", "password", "address", "1234567890", "user@user.com");
        String userJson = "{\"userId\":1,\"userName\":\"name\",\"password\":\"password\",\"postalAddress\":\"address\",\"contactNumber\":\"1234567890\",\"email\":\"user@user.com\"}";
        //when(mockUserRepository.deleteById(1)).thenReturn(true);
        Mockito.when(mockUserRepository.findAll()).thenReturn(new ArrayList<>());

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/allUsers")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }
}