package com.example.usermanagement.service;

import com.example.usermanagement.models.User;
import com.example.usermanagement.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final RabbitTemplate rabbitTemplate;
    private final Exchange exchange;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    public UserService(RabbitTemplate rabbitTemplate, Exchange exchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.exchange = exchange;
    }

    public Boolean CreateUser(User user) throws JsonProcessingException {
        userRepository.save(user);

        User createdUser = userRepository.findByuserName(user.getUserName());
        //send message
        String routingKey = "user.created";
        rabbitTemplate.convertAndSend(exchange.getName(), routingKey, objectMapper.writeValueAsString(createdUser));

        return true;
    }



}
