package com.example.usermanagement.config;

import com.example.usermanagement.service.UserService;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NewUserEventPublisherConfig {

    @Bean
    public Exchange eventExchange() {
        return new TopicExchange("newUserEventExchange");
    }

    @Bean
    public UserService newUserService(RabbitTemplate rabbitTemplate, Exchange eventExchange) {
        return new UserService(rabbitTemplate, eventExchange);
    }
}
