package com.example.usermanagement.controller;

import com.example.usermanagement.models.User;
import com.example.usermanagement.repository.UserRepository;
import com.example.usermanagement.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.ValidationException;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @PostMapping("/user")
    public Boolean createUser(@RequestBody User user) throws JsonProcessingException {
        if (userRepository.existsByuserName(user.getUserName())){
            throw new UserExistsException();
        }

        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userService.CreateUser(user);
        return true;
    }

    @PutMapping("/user")
    public Boolean updateUser(@RequestBody User user){
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    @DeleteMapping("/user/{userId}")
    public Boolean deleteUser(@PathVariable int userId){
        userRepository.deleteById(userId);
        return true;
    }

    @GetMapping("/user")
    public User getUserByID(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userRepository.findByuserName(auth.getName());
    }

    @GetMapping("/allUsers")
    public List<User> listAllUsers(){
        return userRepository.findAll();
    }

    @ResponseStatus(value= HttpStatus.CONFLICT, reason="Username already exists")  // 404
    public class UserExistsException extends ValidationException {
        // ...
    }
}
