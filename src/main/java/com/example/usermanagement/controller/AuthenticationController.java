package com.example.usermanagement.controller;

import com.example.usermanagement.models.AuthenticationResponse;
import com.example.usermanagement.models.User;
import com.example.usermanagement.service.UserAuthenticationService;
import com.example.usermanagement.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AuthenticationController {

    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/user/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody User request) throws Exception {

        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUserName(), request.getPassword()));

        } catch (BadCredentialsException e) {
            throw new Exception("Username or password is incorrect", e);
        }

        final UserDetails userDetails = userAuthenticationService.loadUserByUsername(request.getUserName());
        final String jwt = jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));


    }
}
