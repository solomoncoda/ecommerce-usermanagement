package com.example.usermanagement.repository;

import com.example.usermanagement.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    Boolean existsByuserName(String username);
    User findByuserName(String username);
    User findById(int userId);
    List<User> findAll();
}
