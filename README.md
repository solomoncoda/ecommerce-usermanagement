# eCommerce-UserManagement

Sample eCommerce application with microservice architecture. This repo contains user-management microservice.

## Tech used

* Spring Boot
* MySql
* Angular
* JWT

## ToDo

| Status | Description |
| --- | --- |
|  ☑️ | Create Authentication Controller with "../authenticate" endpoint |
|☑️| Extend the class WebSecurityConfigurerAdapter and configure HttpSecurity.|
|☑️| Override the configure method, disable CSRF, Authorize "../authenticate" endpoint for all users.|
|☑️| Set SessionCreationPolicy as StateLess|
|☑️| Add a new Filter class by extending the OncePerRequestFilter class. It should execute before UsernamePasswordAuthenticationFilter Class|
|☑️| Override the doFilterInternal method inside Filter class.|
|☑️| The doFilterInternal method would get the Authorization Header and validate the JWT Token.|
|☑️| Create a Service class that implements UserDetailsService from SpringFramework.Security package|
|☑️| Override the loadByUserName method, call the Repository's findByUsername method and return the UserDetails.|
|☑️| Create User Controller with necessary endpoints to Create, Update, Delete the user.|